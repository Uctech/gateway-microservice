package com.musala.gatewaymicroservice;

import com.musala.gatewaymicroservice.controllers.DeviceControllerTest;
import com.musala.gatewaymicroservice.controllers.GatewayControllerTest;
import com.musala.gatewaymicroservice.services.DeviceServiceTest;
import com.musala.gatewaymicroservice.services.GatewayServiceTest;

import org.junit.runners.Suite;

@Suite.SuiteClasses({
        GatewayControllerTest.class,
        DeviceControllerTest.class,
        GatewayServiceTest.class,
        DeviceServiceTest.class
})
class GatewayMicroserviceApplicationTests {


}
