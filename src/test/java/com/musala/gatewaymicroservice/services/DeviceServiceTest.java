package com.musala.gatewaymicroservice.services;


import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.repositories.DeviceRepository;
import com.musala.gatewaymicroservice.repositories.GatewayRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Optional;

import static com.musala.gatewaymicroservice.enums.ResponseCode.*;
import static com.musala.gatewaymicroservice.helpers.TestHelper.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;


@Slf4j
@RunWith(SpringRunner.class)
@ContextConfiguration(classes ={DeviceService.class})
@EnableConfigurationProperties
@TestPropertySource("classpath:test.properties")
public class DeviceServiceTest {

    @Autowired
    private DeviceService deviceService;

    @MockBean
    private  DeviceRepository deviceRepository;

    @MockBean
    private  GatewayRepository gatewayRepository;


    @Test
    public void testSaveDeviceSuccess() {
        doReturn(Optional.of(validGatewayResponse(GATEWAY_NAME))).when(gatewayRepository).findById(any());
        doReturn(validDeviceResponse()).when(deviceRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.saveDevice(GATEWAY_ID,validDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(DEVICE_CREATED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testSaveDeviceInvalidStatusFailure() {
        doReturn(Optional.of(validGatewayResponse(GATEWAY_NAME))).when(gatewayRepository).findById(any());
        doReturn(validDeviceResponse()).when(deviceRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.saveDevice(GATEWAY_ID,invalidDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(STATUS_DOES_NOT_EXIST.getCanonicalCode()))
                .verifyComplete();
    }


    @Test
    public void testSaveDeviceBlankVendorFailure() {
        doReturn(Optional.of(validGatewayResponse(GATEWAY_NAME))).when(gatewayRepository).findById(any());
        doReturn(validDeviceResponse()).when(deviceRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.saveDevice(GATEWAY_ID,blankVendorDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(VENDOR_NAME_REQUIRED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testSaveDeviceWithNullGatewayFailure() {

        doReturn(validDeviceResponse()).when(deviceRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.saveDevice(GATEWAY_ID,blankVendorDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }


    @Test
    public void testUpdateDeviceSuccess() {
        doReturn(Optional.of(validDeviceResponse())).when(deviceRepository).findById(any());
        doReturn(validDeviceResponse()).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.updateDevice(1l,validDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(DEVICE_UPDATED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testUpdateDeviceFailure() {
        doReturn(validDeviceResponse()).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.updateDevice(1l,validDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(DEVICE_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testDeleteDeviceSuccess() {
        doReturn(Optional.of(validDeviceResponse())).when(deviceRepository).findById(any());
        Mockito.doNothing().when(deviceRepository).delete(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.deleteDevice(1l);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(DEVICE_DELETED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testDeleteDeviceFailure() {
        Mockito.doNothing().when(deviceRepository).delete(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.deleteDevice(1l);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(DEVICE_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testGetDeviceSuccess() {
        doReturn(Optional.of(validDeviceResponse())).when(deviceRepository).findById(any());
        Mono<GatewayResponse> gatewayResponseMono = deviceService.getDevice(1l);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(OK.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testGetDeviceFailure() {
        Mono<GatewayResponse> gatewayResponseMono = deviceService.getDevice(1l);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(DEVICE_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }

}