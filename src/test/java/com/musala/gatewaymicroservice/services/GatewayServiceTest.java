package com.musala.gatewaymicroservice.services;



import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.repositories.DeviceRepository;
import com.musala.gatewaymicroservice.repositories.GatewayRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;


import java.util.Optional;

import static com.musala.gatewaymicroservice.enums.ResponseCode.*;
import static com.musala.gatewaymicroservice.helpers.TestHelper.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes ={GatewayService.class})
@EnableConfigurationProperties
@TestPropertySource("classpath:test.properties")
@Slf4j
public class GatewayServiceTest {

    @Autowired
    private GatewayService gatewayService;

    @MockBean
    private  GatewayRepository gatewayRepository;

    @MockBean
    private  DeviceRepository deviceRepository;


    @Test
    public void testSaveGatewaySuccess() {
        doReturn(validGatewayResponse(GATEWAY_NAME)).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.saveGateway(validGatewayRequest(GATEWAY_NAME));

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_CREATED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testSaveGatewayInvalidIpv4Failure() {
        doReturn(validGatewayResponse(GATEWAY_NAME)).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.saveGateway(invalidGatewayRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(INVALID_IPV4_ADDRESS.getCanonicalCode()))
                .verifyComplete();
    }
    @Test
    public void testSaveGatewayBlankIpv4Failure() {
        doReturn(validGatewayResponse(GATEWAY_NAME)).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.saveGateway(blankIpv4GatewayRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(IPV4_ADDRESS_REQUIRED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testUpdateGatewaySuccess() {
        doReturn(Optional.of(validGatewayResponse(GATEWAY_NAME_UPDATED))).when(gatewayRepository).findById(any());
        doReturn(validGatewayResponse(GATEWAY_NAME_UPDATED)).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.updateGateway(GATEWAY_ID,validGatewayRequest(GATEWAY_NAME_UPDATED));

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_UPDATED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testUpdateGatewayFailure() {
        doReturn(validGatewayResponse(GATEWAY_NAME_UPDATED)).when(gatewayRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.updateGateway(GATEWAY_ID,validGatewayRequest(GATEWAY_NAME_UPDATED));

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testDeleteGatewaySuccess() {
        doReturn(Optional.of(validGatewayResponse(GATEWAY_NAME))).when(gatewayRepository).findById(any());
        Mockito.doNothing().when(gatewayRepository).delete(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.deleteGateway(GATEWAY_ID);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_DELETED.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testDeleteGatewayFailure() {
        Mockito.doNothing().when(gatewayRepository).delete(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.deleteGateway(GATEWAY_ID);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testSaveAllGatewaySuccess() {
        doReturn(sampleAllGatewaysResponse(OK)).when(gatewayRepository).findAll();
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.fetchAllGateway();

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(OK.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testGetGatewaySuccess() {
        doReturn(Optional.of(validGatewayResponse(GATEWAY_NAME))).when(gatewayRepository).findById(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.getGateway(GATEWAY_ID);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(OK.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testGetGatewayFailure() {
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.getGateway(GATEWAY_ID);

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_NOT_FOUND.getCanonicalCode()))
                .verifyComplete();
    }

    @Test
    public void testSaveGatewayAndDevicesSuccess() {
        doReturn(validGatewayResponse(GATEWAY_NAME)).when(gatewayRepository).saveAndFlush(any());
        doReturn(sampleDevicesResponse()).when(deviceRepository).saveAndFlush(any());
        Mono<GatewayResponse> gatewayResponseMono = gatewayService.saveGatewayAndDevices(invalidGatewayDeviceRequest());

        StepVerifier
                .create(gatewayResponseMono)
                .expectNextMatches(response -> response.getStatusCode().equals(GATEWAY_CREATED.getCanonicalCode()))
                .verifyComplete();
    }
}