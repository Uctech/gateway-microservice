package com.musala.gatewaymicroservice.controllers;

import com.musala.gatewaymicroservice.GatewayMicroserviceApplication;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.services.GatewayService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static com.musala.gatewaymicroservice.enums.ResponseCode.*;
import static com.musala.gatewaymicroservice.helpers.TestHelper.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = GatewayMicroserviceApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@PropertySource("classpath:test.properties")
@Slf4j
public class GatewayControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @SpyBean
    private GatewayService gatewayService;

    @Test
    public void testSaveGatewaySuccess()  {
        GatewayResponse gatewayResponse = sampleGatewayResponseSuccess(OK);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(gatewayService).saveGateway(any());
        webTestClient
                .post()
                .uri(GATEWAY_END_POINT)
                .body(BodyInserters.fromValue(validGatewayRequest(GATEWAY_NAME)))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                });
    }

    @Test
    public void testSaveGatewayAndDevicesSuccess()  {
        GatewayResponse gatewayResponse = sampleGatewayDeviceResponse();
        Mockito.doReturn(Mono.just(gatewayResponse)).when(gatewayService).saveGatewayAndDevices(any());
        webTestClient
                .post()
                .uri(GATEWAY_END_POINT+"/devices")
                .body(BodyInserters.fromValue(invalidGatewayDeviceRequest()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                });
    }

    @Test
    public void testGetGatewaySuccess()  {
        GatewayResponse gatewayResponse = sampleGatewayResponseSuccess(OK);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(gatewayService).getGateway(anyString());
        webTestClient
                .get()
                .uri(String.format(GATEWAY_END_POINT_WITH_SLASH,GATEWAY_ID))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                    Assert.assertNotNull(response.getData());
                });
    }

    @Test
    public void testGetAllGatewaySuccess()  {
        GatewayResponse gatewayResponse = sampleAllGatewaysResponseSuccess(OK);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(gatewayService).fetchAllGateway();
        webTestClient
                .get()
                .uri(GATEWAY_END_POINT)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                    Assert.assertNotNull(response.getData());
                });
    }

    @Test
    public void testUpdateGatewaySuccess()  {
        GatewayResponse gatewayResponse = sampleGatewayResponseSuccess(OK);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(gatewayService).updateGateway(anyString(),any());
        webTestClient
                .put()
                .uri(String.format(GATEWAY_END_POINT_WITH_SLASH,GATEWAY_ID))
                .body(BodyInserters.fromValue(validGatewayRequest(GATEWAY_NAME_UPDATED)))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                });
    }

    @Test
    public void testDeleteGatewaySuccess()  {
        GatewayResponse gatewayResponse = sampleGatewayResponseSuccess(GATEWAY_DELETED);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(gatewayService).deleteGateway(anyString());
        webTestClient
                .delete()
                .uri(String.format(GATEWAY_END_POINT_WITH_SLASH,GATEWAY_ID))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , GATEWAY_DELETED.getCanonicalCode());
                });
    }
}