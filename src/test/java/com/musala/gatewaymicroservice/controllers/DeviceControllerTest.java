package com.musala.gatewaymicroservice.controllers;

import com.musala.gatewaymicroservice.GatewayMicroserviceApplication;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.services.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import static com.musala.gatewaymicroservice.enums.ResponseCode.*;
import static com.musala.gatewaymicroservice.helpers.TestHelper.*;
import static org.mockito.ArgumentMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = GatewayMicroserviceApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@PropertySource("classpath:test.properties")
@Slf4j
public class DeviceControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @SpyBean
    private DeviceService deviceService;

    @Test
    public void testSaveDeviceSuccess()  {
        var gatewayResponse = sampleDeviceResponseSuccess(DEVICE_CREATED);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(deviceService).saveDevice(anyString(),any());
        webTestClient
                .post()
                .uri(String.format(DEVICE_END_POINT+"/gateway/%s",GATEWAY_ID))
                .body(BodyInserters.fromValue(validDeviceRequest()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , DEVICE_CREATED.getCanonicalCode());
                });
    }

    @Test
    public void testGetDeviceSuccess()  {
        GatewayResponse gatewayResponse = sampleDeviceResponseSuccess(OK);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(deviceService).getDevice(anyLong());
        webTestClient
                .get()
                .uri(DEVICE_END_POINT+"/1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                    Assert.assertNotNull(response.getData());
                });
    }

    @Test
    public void testDeleteDeviceSuccess()  {
        GatewayResponse gatewayResponse = new GatewayResponse(DEVICE_DELETED);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(deviceService).deleteDevice(anyLong());
        webTestClient
                .delete()
                .uri(DEVICE_END_POINT+"/1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , DEVICE_DELETED.getCanonicalCode());
                });
    }
    @Test
    public void testUpdateDeviceSuccess()  {
        GatewayResponse gatewayResponse = sampleGatewayResponseSuccess(OK);
        Mockito.doReturn(Mono.just(gatewayResponse)).when(deviceService).updateDevice(anyLong(),any());
        webTestClient
                .put()
                .uri(String.format(DEVICE_END_POINT+"/1"))
                .body(BodyInserters.fromValue(validDeviceRequest()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(GatewayResponse.class)
                .value(response -> {
                    Assert.assertNotNull(response);
                    Assert.assertEquals(response.getStatusCode() , OK.getCanonicalCode());
                });
    }
}