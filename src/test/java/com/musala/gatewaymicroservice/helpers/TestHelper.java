package com.musala.gatewaymicroservice.helpers;


import com.musala.gatewaymicroservice.dto.request.DeviceRequest;
import com.musala.gatewaymicroservice.dto.request.GatewayDeviceRequest;
import com.musala.gatewaymicroservice.dto.request.GatewayRequest;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.entities.Device;
import com.musala.gatewaymicroservice.entities.Gateway;
import com.musala.gatewaymicroservice.enums.ResponseCode;
import com.musala.gatewaymicroservice.enums.Status;

import java.util.List;

import static com.musala.gatewaymicroservice.enums.ResponseCode.OK;


public class TestHelper {
    public static final String GATEWAY_END_POINT = "/api/v1/gateways";
    public static final String DEVICE_END_POINT = "/api/v1/devices";
    public static final String GATEWAY_END_POINT_WITH_SLASH = "/api/v1/gateways/%s";
    public static final String GATEWAY_ID = "1690d034-ab70-48b9-bcd9-72209a2889dc";
    public static final String GATEWAY_NAME = "Gateway test";
    public static final String GATEWAY_NAME_UPDATED = "Gateway test Updated";
    public static final String VENDOR_NAME = "CISCO";


    public static GatewayResponse sampleGatewayResponseSuccess(ResponseCode responseCode){
       return  new GatewayResponse(responseCode,validGatewayResponse(GATEWAY_NAME));
    }

    public static GatewayResponse sampleDeviceResponseSuccess(ResponseCode responseCode){
        return  new GatewayResponse(responseCode,validDeviceResponse());
    }

    public static GatewayResponse sampleGatewayResponseUnsuccessful(ResponseCode responseCode){
        return  new GatewayResponse(responseCode,invalidGatewayResponse());
    }

    public static Gateway validGatewayResponse(String gatewayName){
        return Gateway.builder()
                .ipv4Address("192.0.2.144")
                .name(gatewayName)
                .id(GATEWAY_ID)
                .devices(List.of())
                .build();
    }

    public static Gateway invalidGatewayResponse(){
        return Gateway.builder()
                .ipv4Address("192.0.2")
                .name("Gateway test")
                .id(GATEWAY_ID)
                .build();
    }
    public static Gateway invalidGatewayIdResponse(){
        return Gateway.builder()
                .ipv4Address("192.0.2.144")
                .name("Gateway test")
                .build();
    }
    public static GatewayRequest invalidGatewayRequest(){
        return GatewayRequest.builder()
                .ipv4Address("192.0.2")
                .name("Gateway test")
                .build();
    }

    public static GatewayRequest blankIpv4GatewayRequest(){
        return GatewayRequest.builder()
                .ipv4Address("")
                .name("Gateway test")
                .build();
    }

    public static GatewayRequest validGatewayRequest(String gatewayName){
        return GatewayRequest.builder()
                .ipv4Address("192.0.2.144")
                .name(gatewayName)
                .build();
    }

    public static GatewayResponse sampleAllGatewaysResponseSuccess(ResponseCode responseCode){
        var gatewayRequests = List.of(Gateway.builder()
                        .id(GATEWAY_ID)
                        .ipv4Address("192.0.2.144")
                        .name(GATEWAY_NAME)
                        .build(),
                Gateway.builder()
                        .id("4690d034-ab70-48b9-bcd9-72209a2889dd")
                        .ipv4Address("192.0.2.143")
                        .name("Gateway test2")
                        .build()
        );
        return  new GatewayResponse(responseCode,gatewayRequests);
    }

    public static List<Gateway> sampleAllGatewaysResponse(ResponseCode responseCode){
        return  List.of(Gateway.builder()
                        .id(GATEWAY_ID)
                        .ipv4Address("192.0.2.144")
                        .name(GATEWAY_NAME)
                        .build(),
                Gateway.builder()
                        .id("4690d034-ab70-48b9-bcd9-72209a2889dd")
                        .ipv4Address("192.0.2.143")
                        .name("Gateway test2")
                        .build()
        );

    }

    public static List<DeviceRequest> sampleGatewayDeviceRequest(){
       return List.of(DeviceRequest.builder()
                        .status(Status.OFF_LINE.getValue())
                        .vendor(VENDOR_NAME)
                        .build()
       );

    }
    public static GatewayResponse sampleGatewayDeviceResponse(){
       var devices = List.of(Device.builder()
                        .id(1l)
                        .status(Status.OFF_LINE.getValue())
                        .vendor(VENDOR_NAME)
                        .build()
        );
       var gateway = Gateway.builder()
               .id(GATEWAY_ID)
               .ipv4Address("192.0.2.144")
               .name(GATEWAY_NAME)
               .devices(devices)
               .build();

        return  new GatewayResponse(OK,gateway);
    }

    public static List<Device> sampleDevicesResponse(){
        return List.of(Device.builder()
                .id(1l)
                .status(Status.OFF_LINE.getValue())
                .vendor(VENDOR_NAME)
                .gateway(validGatewayResponse(GATEWAY_NAME))
                .build()
        );
    }

    public static GatewayDeviceRequest invalidGatewayDeviceRequest(){
        return GatewayDeviceRequest.builder()
                .gatewayRequest(validGatewayRequest(GATEWAY_NAME))
                .deviceRequests(sampleGatewayDeviceRequest())
                .build();
    }

    public static Device validDeviceResponse(){
        return Device.builder()
                .id(1l)
                .gateway(validGatewayResponse(GATEWAY_NAME))
                .vendor(VENDOR_NAME)
                .status(Status.ON_LINE.getValue())
                .build();
    }

    public static DeviceRequest invalidDeviceRequest(){
        return DeviceRequest.builder()
                .vendor(VENDOR_NAME)
                .status("wrong")
                .build();
    }

    public static DeviceRequest blankVendorDeviceRequest(){
        return DeviceRequest.builder()
                .vendor("")
                .status(Status.ON_LINE.getValue())
                .build();
    }

    public static DeviceRequest validDeviceRequest(){
        return DeviceRequest.builder()
                .vendor(VENDOR_NAME)
                .status(Status.ON_LINE.getValue())
                .build();
    }
}
