package com.musala.gatewaymicroservice.repositories;

import com.musala.gatewaymicroservice.entities.Gateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewayRepository extends JpaRepository<Gateway, String> {
}
