package com.musala.gatewaymicroservice.enums;

import lombok.Getter;

@Getter
public enum Status {

    ON_LINE("online"),
    OFF_LINE("offline");

    private String value;

    Status(String value){
        this.value = value;
    }

    /**
     *
     * @param value the value to check if it exists in the enum
     * @return boolean
     */
    public static boolean getStatus(String value){
        for (Status status: values()) {
            if (status.getValue().equalsIgnoreCase(value)){
                return true;
            }
        }
        return false;
    }
}
