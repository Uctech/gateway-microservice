package com.musala.gatewaymicroservice.enums;

import lombok.Getter;
import org.springframework.http.HttpStatus;


@Getter
public enum ResponseCode {

    OK( "0000", "Success", HttpStatus.OK),
    GATEWAY_NOT_FOUND( "4004","Gateway not found", HttpStatus.NOT_FOUND),
    DEVICE_NOT_FOUND( "4004","Device not found", HttpStatus.NOT_FOUND),
    GATEWAY_DELETED( "1013","Gateway deleted successfully", HttpStatus.OK),
    DEVICE_DELETED("1013","Device deleted successfully", HttpStatus.OK),
    GATEWAY_CREATED("2001","Gateway created successfully", HttpStatus.CREATED),
    DEVICE_CREATED( "3008","Device created successfully", HttpStatus.CREATED),
    GATEWAY_UPDATED( "1018","Gateway updated successfully", HttpStatus.OK),
    DEVICE_UPDATED( "1018","Device update successfully", HttpStatus.OK),
    BAD_REQUEST("1031", "Invalid request", HttpStatus.BAD_REQUEST),
    INTERNAL_SERVER_ERROR( "5000", "There was an error while processing the request.", HttpStatus.INTERNAL_SERVER_ERROR),
    STATUS_DOES_NOT_EXIST("4000", "Status does not exist", HttpStatus.NOT_FOUND),
    DEVICE_ALLOWED_EXCEEDED( "4001","Device allowed to save has exceeded", HttpStatus.UNAUTHORIZED),
    INVALID_IPV4_ADDRESS( "4000","Invalid IPv4 address", HttpStatus.BAD_REQUEST),
    IPV4_ADDRESS_REQUIRED("4000","IPv4 address is required", HttpStatus.BAD_REQUEST),
    INVALID_DEVICE_REQUEST("4000","One or more device request is invalid", HttpStatus.BAD_REQUEST),
    VENDOR_NAME_REQUIRED("4000","Vendor name is required", HttpStatus.BAD_REQUEST);

    ResponseCode(
             final String canonicalCode,
            final String description, final HttpStatus httpStatus
    ) {
        this.canonicalCode = canonicalCode;
        this.description = description;
        this.httpStatus = httpStatus;
    }

    private String canonicalCode;
    private String description;
    private HttpStatus httpStatus;

}
