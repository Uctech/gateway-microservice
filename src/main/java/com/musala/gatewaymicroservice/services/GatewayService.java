package com.musala.gatewaymicroservice.services;

import com.musala.gatewaymicroservice.dto.request.DeviceRequest;
import com.musala.gatewaymicroservice.dto.request.GatewayDeviceRequest;
import com.musala.gatewaymicroservice.dto.request.GatewayRequest;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.enums.Status;
import com.musala.gatewaymicroservice.entities.Device;
import com.musala.gatewaymicroservice.entities.Gateway;
import com.musala.gatewaymicroservice.repositories.DeviceRepository;
import com.musala.gatewaymicroservice.repositories.GatewayRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.musala.gatewaymicroservice.constants.CommonConstants.ALLOWED_GATEWAY_DEVICE;
import static com.musala.gatewaymicroservice.enums.ResponseCode.*;

@Slf4j
@Service
public class GatewayService {

    private final GatewayRepository gatewayRepository;
    private final DeviceRepository deviceRepository;

    public GatewayService(GatewayRepository gatewayRepository, DeviceRepository deviceRepository) {
        this.gatewayRepository = gatewayRepository;
        this.deviceRepository = deviceRepository;
    }

    /**
     * Save gateway
     * @param gatewayRequest the dto containing the details to save a gateway
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> saveGateway(GatewayRequest gatewayRequest){

       var gatewayResponseMono = validateIpv4Address(gatewayRequest);

        if (!Objects.isNull(gatewayResponseMono)){
            return gatewayResponseMono;
        }

        var gateway = Gateway.builder()
                .ipv4Address(gatewayRequest.getIpv4Address())
                .name(gatewayRequest.getName())
                .build();
        try{
            Gateway saveGateway = gatewayRepository.saveAndFlush(gateway);
            return Mono.just(new GatewayResponse(GATEWAY_CREATED,saveGateway));
        }catch (Exception ex){
            log.error("Exception occurred while creating gateway {}", ex.getStackTrace());
            return Mono.just(new GatewayResponse(INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * Validate ipv4 address in gateway request
     * @param gatewayRequest the dto containing the details of gateway to validate
     * @return {@link Mono<GatewayResponse>}
     */
    private Mono<GatewayResponse> validateIpv4Address(GatewayRequest gatewayRequest) {
        InetAddressValidator validator = InetAddressValidator.getInstance();

        if(StringUtils.isBlank(gatewayRequest.getIpv4Address())){
            return Mono.just(new GatewayResponse(IPV4_ADDRESS_REQUIRED));
        }

        if (!validator.isValidInet4Address(gatewayRequest.getIpv4Address())){
            return Mono.just(new GatewayResponse(INVALID_IPV4_ADDRESS));
        }
        return null;
    }

    /**
     * Retrieve gateway
     * @param id the gateway id
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> getGateway(String id){

        var gateway = gatewayRepository.findById(id);
        if(gateway.isEmpty()){
            return Mono.just(new GatewayResponse(GATEWAY_NOT_FOUND));
        }

        return Mono.just(new GatewayResponse(OK,gateway));
    }

    /**
     * Update gateway
     * @param id the gateway id
     * @param gatewayRequest the dto containing the details to save a gateway
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> updateGateway(String id,GatewayRequest gatewayRequest){
        var gateway = gatewayRepository.findById(id);
        if(gateway.isEmpty()){
            return Mono.just(new GatewayResponse(GATEWAY_NOT_FOUND));
        }
        var gatewayResponseMono = validateIpv4Address(gatewayRequest);

        if (!Objects.isNull(gatewayResponseMono)){
            return gatewayResponseMono;
        }

        var updateGateway = Gateway.builder()
                .id(id)
                .ipv4Address(gatewayRequest.getIpv4Address())
                .name(gatewayRequest.getName())
                .build();
        try{
            Gateway updatedGateway = gatewayRepository.saveAndFlush(updateGateway);
            return Mono.just(new GatewayResponse(GATEWAY_UPDATED,updatedGateway));
        }catch (Exception ex){
            log.error("Exception occurred while creating gateway {}", ex.getMessage());
            return Mono.just(new GatewayResponse(INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * Delete gateway
     * @param id the gateway id to delete
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> deleteGateway(String id){
        var gateway = gatewayRepository.findById(id);
        if (gateway.isEmpty()){
            return Mono.just(new GatewayResponse(GATEWAY_NOT_FOUND));
        }
        gatewayRepository.delete(gateway.get());
        return Mono.just(new GatewayResponse(GATEWAY_DELETED));

    }

    /**
     * Fetch all gateways
     * @return {@link Mono<GatewayResponse>}
     */
     public Mono<GatewayResponse> fetchAllGateway(){
        return Mono.just(new GatewayResponse(OK,gatewayRepository.findAll()));
    }

    /**
     * Save gateway and devices
     * @param gatewayDeviceRequest the dto containing the details to save a gateway and devices
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> saveGatewayAndDevices(GatewayDeviceRequest gatewayDeviceRequest){

        var gatewayResponseMono = validateIpv4Address(gatewayDeviceRequest.getGatewayRequest());

        if (!Objects.isNull(gatewayResponseMono)){
            return gatewayResponseMono;
        }
        try{
        var gateway = Gateway.builder()
                .ipv4Address(gatewayDeviceRequest.getGatewayRequest().getIpv4Address())
                .name(gatewayDeviceRequest.getGatewayRequest().getName())
                .build();
        var savedGateway = gatewayRepository.saveAndFlush(gateway);

        if(gatewayDeviceRequest.getDeviceRequests().size() >  ALLOWED_GATEWAY_DEVICE-1){
            return Mono.just(new GatewayResponse(DEVICE_ALLOWED_EXCEEDED));
        }
        if(!validateDeviceRequest(gatewayDeviceRequest.getDeviceRequests())){
            return Mono.just(new GatewayResponse(INVALID_DEVICE_REQUEST));
        }
         List<Device> devices = new ArrayList<>();
        for (DeviceRequest deviceRequest : gatewayDeviceRequest.getDeviceRequests()) {
            var device = Device.builder()
                    .vendor(deviceRequest.getVendor())
                    .status(deviceRequest.getStatus())
                    .gateway(savedGateway)
                    .build();
            devices.add(device);
        }

            var savedDeices = deviceRepository.saveAllAndFlush(devices);

            return Mono.just(new GatewayResponse(GATEWAY_CREATED,savedDeices));
        }catch (Exception ex){
            log.error("Exception occurred while creating gateway {}", ex.getMessage());
            return Mono.just(new GatewayResponse(INTERNAL_SERVER_ERROR));
        }
    }

    private  boolean validateDeviceRequest(List<DeviceRequest> deviceRequestList) {
        boolean valid = false;
        for (DeviceRequest deviceRequest : deviceRequestList) {
            if (StringUtils.isNotBlank(deviceRequest.getStatus())) {
                valid = true;
            }
            if (StringUtils.isNotBlank(deviceRequest.getVendor())) {
                valid = true;
            }
            if (Status.getStatus(deviceRequest.getStatus())) {
                valid = true;
            }
        }
        return valid;
    }

}
