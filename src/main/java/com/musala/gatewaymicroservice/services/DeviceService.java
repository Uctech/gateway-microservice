package com.musala.gatewaymicroservice.services;


import com.musala.gatewaymicroservice.dto.request.DeviceRequest;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.enums.Status;
import com.musala.gatewaymicroservice.entities.Device;
import com.musala.gatewaymicroservice.repositories.DeviceRepository;
import com.musala.gatewaymicroservice.repositories.GatewayRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;


import static com.musala.gatewaymicroservice.constants.CommonConstants.ALLOWED_GATEWAY_DEVICE;
import static com.musala.gatewaymicroservice.enums.ResponseCode.*;

@Slf4j
@Service
public class DeviceService {

    private final DeviceRepository deviceRepository;
    private final GatewayRepository gatewayRepository;

    public DeviceService(DeviceRepository deviceRepository, GatewayRepository gatewayRepository) {
        this.deviceRepository = deviceRepository;
        this.gatewayRepository = gatewayRepository;
    }

    /**
     * Save a device
     * @param gatewayId the gateway id required to be used as the foreign key in device entity
     * @param deviceRequest the dto containing the details to save a device
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> saveDevice(String gatewayId,DeviceRequest deviceRequest){
        var gateway = gatewayRepository.findById(gatewayId);

        if(gateway.isEmpty()){
            return Mono.just(new GatewayResponse(GATEWAY_NOT_FOUND));
        }

        if (StringUtils.isBlank(deviceRequest.getVendor())) {
            return Mono.just(new GatewayResponse(VENDOR_NAME_REQUIRED));
        }

        if(!Status.getStatus(deviceRequest.getStatus())){
            return Mono.just(new GatewayResponse(STATUS_DOES_NOT_EXIST));
        }

        if(gateway.get().getDevices().size() > ALLOWED_GATEWAY_DEVICE-1){
            return Mono.just(new GatewayResponse(DEVICE_ALLOWED_EXCEEDED));
        }

        var saveDevice = Device.builder()
                .gateway(gateway.get())
                .status(deviceRequest.getStatus().toLowerCase())
                .vendor(deviceRequest.getVendor())
                .build();
        try {
            var device = deviceRepository.saveAndFlush(saveDevice);
            return Mono.just(new GatewayResponse(DEVICE_CREATED, device));
        }catch (Exception ex){
            log.error("Exception occurred while creating device {}", ex.getMessage());
            return Mono.just(new GatewayResponse(INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * Retrieve a device
     * @param id the device id
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> getDevice(long id){
        var device = deviceRepository.findById(id);

        if(device.isEmpty()){

            return Mono.just(new GatewayResponse(DEVICE_NOT_FOUND));
        }
        return Mono.just(new GatewayResponse(OK,device.get()));
    }

    /**
     * Update a device
     * @param deviceId the device id
     * @param deviceRequest the dto containing the details to update a device
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> updateDevice( long deviceId, DeviceRequest deviceRequest){
        var device = deviceRepository.findById(deviceId);
        if(device.isEmpty()){
            return Mono.just(new GatewayResponse(DEVICE_NOT_FOUND));
        }

        var updateDevice = Device.builder()
                .id(deviceId)
                .gateway(device.get().getGateway())
                .status(deviceRequest.getStatus().toLowerCase())
                .vendor(deviceRequest.getVendor())
                .build();
        try{
            var updatedDevice = deviceRepository.saveAndFlush(updateDevice);
            return Mono.just(new GatewayResponse(DEVICE_UPDATED,updatedDevice));
        }catch (Exception ex){
            log.error("Exception occurred while creating device {}", ex.getMessage());
            return Mono.just(new GatewayResponse(INTERNAL_SERVER_ERROR));
        }
    }

    /**
     * Delete a device
     * @param id the device id
     * @return {@link Mono<GatewayResponse>}
     */
    public Mono<GatewayResponse> deleteDevice(long id){
        var device = deviceRepository.findById(id);
        if (device.isEmpty()){
            return Mono.just(new GatewayResponse(DEVICE_NOT_FOUND));
        }
        deviceRepository.delete(device.get());
        return Mono.just(new GatewayResponse(DEVICE_DELETED));
    }
}
