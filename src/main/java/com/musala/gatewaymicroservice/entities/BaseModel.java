package com.musala.gatewaymicroservice.entities;


import lombok.*;


import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostUpdate;
import java.time.LocalDateTime;


@Getter
@Setter
@ToString
@NoArgsConstructor
@MappedSuperclass
public abstract class BaseModel<T> {

    @Column(name = "created_at")
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "modified_at")
    private LocalDateTime modifiedAt;

    @PostUpdate
    private void update() {
        setModifiedAt(LocalDateTime.now());
    }
}
