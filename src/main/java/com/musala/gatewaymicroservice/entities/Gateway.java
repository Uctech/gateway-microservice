package com.musala.gatewaymicroservice.entities;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Data
@Builder
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "gateway")
public class Gateway extends BaseModel<Gateway> {

    @Id
    private String id;

    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "IPv4 address cannot be null")
    @Column(name = "ipv4_address")
    private String ipv4Address;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gateway")
    private List<Device> devices = new ArrayList<>();

    @PrePersist
    private void ensureId(){
        this.setId(UUID.randomUUID().toString());
    }

}
