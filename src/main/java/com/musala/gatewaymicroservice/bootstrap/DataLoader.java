package com.musala.gatewaymicroservice.bootstrap;

import com.musala.gatewaymicroservice.entities.Device;
import com.musala.gatewaymicroservice.entities.Gateway;
import com.musala.gatewaymicroservice.repositories.DeviceRepository;
import com.musala.gatewaymicroservice.repositories.GatewayRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class DataLoader  implements ApplicationListener<ContextRefreshedEvent> {

    private Logger log = LogManager.getLogger(DataLoader.class);

    @Autowired
    private GatewayRepository gatewayRepository;

    @Autowired
    private DeviceRepository deviceRepository;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        seedData();
    }

    private void seedData() {
            var gateWayList = List.of(
                    Gateway.builder()
                            .name("Media gateways")
                            .ipv4Address("192.0.2.143")
                            .build(),
                    Gateway.builder()
                            .name("Payment gateways")
                            .ipv4Address("192.0.2.143")
                            .build(),
                    Gateway.builder()
                            .name("WAP Network Gateways")
                            .ipv4Address("192.0.2.143")
                            .build()
            );

          var savedGateways=  gatewayRepository.saveAll(gateWayList);

        var deviceList = List.of(
                Device.builder()
                        .status("online")
                        .vendor("iboss")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("online")
                        .vendor("Symantec")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("online")
                        .vendor("F5 Networks")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("online")
                        .vendor("McAfee")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("online")
                        .vendor("zScaler")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("online")
                        .vendor("Cisco")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("offline")
                        .vendor("Barracuda")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("offline")
                        .vendor("Forcepoint")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("offline")
                        .vendor("Check Point Software")
                        .gateway(savedGateways.get(1))
                        .build(),
                Device.builder()
                        .status("offline")
                        .vendor("Denk ")
                        .gateway(savedGateways.get(2))
                        .build()
        );

        deviceRepository.saveAll(deviceList);
    }

}
