package com.musala.gatewaymicroservice.exception;

import lombok.Data;

@Data
public class GatewayException extends RuntimeException{

    private final Integer httpCode;
    private String statusCode;

   public GatewayException(Integer httpCode, String message, String statusCode){
       super(message);
       this.httpCode = httpCode;
       this.statusCode = statusCode;
   }
}
