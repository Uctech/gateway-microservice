package com.musala.gatewaymicroservice.exception;

import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import static com.musala.gatewaymicroservice.enums.ResponseCode.BAD_REQUEST;
import static com.musala.gatewaymicroservice.enums.ResponseCode.INTERNAL_SERVER_ERROR;

@Slf4j
@ResponseBody
@ControllerAdvice(annotations = RestController.class, basePackages ="com.musala.gatewaymicroservice.controllers")
public class AdviceControllerThrowable {

    @ExceptionHandler(NullPointerException.class)
    public GatewayResponse noAccessException(NullPointerException ex){
        log.error("Null pointer Exception", ex);
        return new GatewayResponse(INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public GatewayResponse noAccessException(MethodArgumentNotValidException ex){
        log.error("MethodArgumentNotValidException", ex);
        return new GatewayResponse(BAD_REQUEST);
    }

    public GatewayResponse noAccessException(Exception ex){
        log.error("Unknown Exception", ex);
        return new GatewayResponse(INTERNAL_SERVER_ERROR);
    }
}
