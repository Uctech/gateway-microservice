package com.musala.gatewaymicroservice.configurations;

import com.musala.gatewaymicroservice.constants.CommonConstants;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info().title("Gateway-devices Microservice").version(CommonConstants.API_VERSION_NUMBER))
                .addSecurityItem(new SecurityRequirement().addList("Authorization"));
    }

}
