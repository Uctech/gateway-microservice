package com.musala.gatewaymicroservice.constants;

public interface CommonConstants {
    String API_VERSION = "/api/v1/";
    String API_VERSION_NUMBER = "1.0.0";
    int ALLOWED_GATEWAY_DEVICE = 10;
}
