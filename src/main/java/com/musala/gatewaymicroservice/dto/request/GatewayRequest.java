package com.musala.gatewaymicroservice.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GatewayRequest {

    @NotNull(message = "IPv4 address cannot be null")
    private String ipv4Address;

    @NotNull(message = "name  cannot be null")
    private String name;
}
