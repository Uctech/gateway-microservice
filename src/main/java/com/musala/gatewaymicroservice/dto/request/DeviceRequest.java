package com.musala.gatewaymicroservice.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceRequest {

    @NotNull(message = "Vendor name is required")
    private String vendor;
    @NotNull
    private String status;
}
