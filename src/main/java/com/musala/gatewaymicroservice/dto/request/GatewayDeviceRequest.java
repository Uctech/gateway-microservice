package com.musala.gatewaymicroservice.dto.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GatewayDeviceRequest {

    private GatewayRequest gatewayRequest;

    private List<DeviceRequest> deviceRequests;
}
