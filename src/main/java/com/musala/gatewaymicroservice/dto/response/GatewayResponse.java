package com.musala.gatewaymicroservice.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musala.gatewaymicroservice.enums.ResponseCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GatewayResponse {
    private String statusCode;
    private String statusMessage;
    private String timestamp;
    private Object data;

    public GatewayResponse(ResponseCode responseCode){
        this.statusCode = responseCode.getCanonicalCode();
        this.statusMessage = responseCode.getDescription();
        this.timestamp = LocalDateTime.now().toString();
    }

    public GatewayResponse(ResponseCode responseCode, Object data){
        this.statusCode = responseCode.getCanonicalCode();
        this.statusMessage = responseCode.getDescription();
        this.timestamp = LocalDateTime.now().toString();
        this.data = data;
    }
}
