package com.musala.gatewaymicroservice.controllers;

import com.musala.gatewaymicroservice.constants.CommonConstants;
import com.musala.gatewaymicroservice.dto.request.GatewayDeviceRequest;
import com.musala.gatewaymicroservice.dto.request.GatewayRequest;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.services.GatewayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(CommonConstants.API_VERSION+"gateways")
public class GatewayController {

    private GatewayService gatewayService;

    public GatewayController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<GatewayResponse>> createGateway(@Valid @RequestBody GatewayRequest gatewayRequest){
        return gatewayService.saveGateway(gatewayRequest)
                .map(ResponseEntity::ok);
    }

    @GetMapping(path = "/{id}")
    public Mono<ResponseEntity<GatewayResponse>> getGateway(@PathVariable String id){
        return gatewayService.getGateway(id)
                .map(ResponseEntity::ok);
    }

    @PutMapping(path = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<GatewayResponse>> updateGateway(@PathVariable String id, @Valid @RequestBody GatewayRequest gatewayRequest){
        return gatewayService.updateGateway(id, gatewayRequest)
                .map(ResponseEntity::ok);
    }

    @DeleteMapping(path = "/{id}")
    public Mono<ResponseEntity<GatewayResponse>> deleteGateway(@PathVariable String id){
        return gatewayService.deleteGateway(id)
                .map(ResponseEntity::ok);
    }

    @GetMapping
    public Mono<ResponseEntity<GatewayResponse>> getAllGateway(){
        return gatewayService.fetchAllGateway()
                .map(ResponseEntity::ok);
    }

    @PostMapping(path="/devices",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<GatewayResponse>> createGatewayAndDevices(@Valid @RequestBody GatewayDeviceRequest request){
        return gatewayService.saveGatewayAndDevices(request)
                .map(ResponseEntity::ok);
    }
}
