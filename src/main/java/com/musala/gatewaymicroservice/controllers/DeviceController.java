package com.musala.gatewaymicroservice.controllers;

import com.musala.gatewaymicroservice.constants.CommonConstants;
import com.musala.gatewaymicroservice.dto.request.DeviceRequest;
import com.musala.gatewaymicroservice.dto.response.GatewayResponse;
import com.musala.gatewaymicroservice.services.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(CommonConstants.API_VERSION+"devices")
public class DeviceController {
    private DeviceService deviceService;

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }


    @PostMapping(path = "/gateway/{gatewayId}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<GatewayResponse>> createDevice(@PathVariable String gatewayId, @Valid @RequestBody DeviceRequest deviceRequest){
        return deviceService.saveDevice(gatewayId,deviceRequest)
                .map(ResponseEntity::ok);
    }

    @GetMapping(path = "/{id}")
    public Mono<ResponseEntity<GatewayResponse>> getDevice(@PathVariable long id){
        return deviceService.getDevice(id)
                .map(ResponseEntity::ok);
    }

    @PutMapping(path = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<GatewayResponse>> updateDevice(@PathVariable long id, @Valid @RequestBody DeviceRequest deviceRequest){
        return deviceService.updateDevice(id, deviceRequest)
                .map(ResponseEntity::ok);
    }

    @DeleteMapping(path = "/{id}")
    public Mono<ResponseEntity<GatewayResponse>> deleteDevice(@PathVariable long id){
        return deviceService.deleteDevice(id)
                .map(ResponseEntity::ok);
    }
}
