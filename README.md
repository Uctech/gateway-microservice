# Gateways MicroService

It is responsible for Gateways and peripheral devices Management. users can
perform Create, Read Update and Delete (CRUD) operations, a gateway can only accept ten peripheral devices.

## Running the project locally

To run the project locally you need to:

1. Have H2 in-memory database installed and running;

2. Create the db "musala" on H2 database.

3. Update the dev.properties file with your H2 database credentials

4. Set spring.profiles.active=dev in the application.properties file

5. Run the project

6. Open your browser and go to "http://localhost:8081/swagger-ui.html" to access the
   swagger doc from where you can test the endpoints.


## Executing the Unit Tests
To execute the unit tests simply run  ``` > mvn clean package``` on the terminal and allow
it to execute, or you can navigate to the test package and execute the tests from there.

## Test Coverage
Jacoco is used for test coverage.

To see the test coverage, run ``` > mvn clean package ```. When it's done, navigate
to ```target > site > jacoco ```, you will see an index.html file. Open the html
page in your browser to see the test coverage.


## Keep in touch
Please reach out to me for any question or clarification (Livinus Mamah).

